<?php
declare(strict_types=1);

namespace JournoLink\Standards\Fixers;

use PhpCsFixer\Fixer\FixerInterface;

abstract class AbstractFixer implements FixerInterface
{
    final public static function name(): string
    {
        return 'JournoLink/' . \implode(
            '_',
            \array_map(
                'strtolower',
                \preg_split(
                    '/(?=[A-Z])/',
                    \preg_replace(
                        '/^.*\\\\([a-zA-Z0-1]+)Fixer$/',
                        '$1',
                        static::class
                    ),
                    0,
                    PREG_SPLIT_NO_EMPTY
                )
            )
        );
    }

    final public function getName(): string
    {
        return self::name();
    }

    final public function supports(\SplFileInfo $file): bool
    {
        return true;
    }
}
