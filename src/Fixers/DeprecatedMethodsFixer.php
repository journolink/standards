<?php
declare(strict_types=1);

namespace JournoLink\Standards\Fixers;

use PhpCsFixer\Tokenizer\Token;
use PhpCsFixer\Tokenizer\Tokens;
use PhpCsFixer\FixerDefinition\CodeSample;
use PhpCsFixer\FixerDefinition\FixerDefinition;

final class DeprecatedMethodsFixer extends AbstractFixer
{
    /** @var array */
    protected $deprecations = [
        'array_add' => 'Arr::add',
        'array_collapse' => 'Arr::collapse',
        'array_divide' => 'Arr::divide',
        'array_dot' => 'Arr::dot',
        'array_except' => 'Arr::except',
        'array_first' => 'Arr::first',
        'array_flatten' => 'Arr::flatten',
        'array_forget' => 'Arr::forget',
        'array_get' => 'Arr::get',
        'array_has' => 'Arr::has',
        'array_last' => 'Arr::last',
        'array_only' => 'Arr::only',
        'array_pluck' => 'Arr::pluck',
        'array_prepend' => 'Arr::prepend',
        'array_pull' => 'Arr::pull',
        'array_random' => 'Arr::random',
        'array_set' => 'Arr::set',
        'array_sort' => 'Arr::sort',
        'array_sort_recursive' => 'Arr::sortRecursive',
        'array_where' => 'Arr::where',
        'array_wrap' => 'Arr::wrap',
        'camel_case' => 'Str::camel',
        'ends_with' => 'Str::endsWith',
        'kebab_case' => 'Str::kebab',
        'snake_case' => 'Str::snake',
        'starts_with' => 'Str::startsWith',
        'str_after' => 'Str::after',
        'str_before' => 'Str::before',
        'str_contains' => 'Str::contains',
        'str_finish' => 'Str::finish',
        'str_is' => 'Str::is',
        'str_limit' => 'Str::limit',
        'str_plural' => 'Str::plural',
        'str_random' => 'Str::random',
        'str_replace_array' => 'Str::replaceArray',
        'str_replace_first' => 'Str::replaceFirst',
        'str_replace_last' => 'Str::replaceLast',
        'str_singular' => 'Str::singular',
        'str_slug' => 'Str::slug',
        'str_start' => 'Str::start',
        'studly_case' => 'Str::studly',
        'title_case' => 'Str::title',
    ];

    public function getDefinition(): FixerDefinition
    {
        return new FixerDefinition(
            'Laravel helper method is deprecated.',
            [new CodeSample('')]
        );
    }

    public function isCandidate(Tokens $tokens): bool
    {
        return $tokens->isAnyTokenKindsFound([T_FUNCTION]);
    }

    public function isRisky(): bool
    {
        return false;
    }

    public function fix(\SplFileInfo $file, Tokens $tokens): void
    {
        $prefixes = \array_unique(\array_map(function ($key) {
            return \substr($key, 0, \stripos($key, '_'));
        }, \array_keys($this->deprecations)));

        foreach ($tokens as $index => $token) {
            if ($this->startsWith($token->getContent(), $prefixes) && \in_array($token->getContent(), array_keys($this->deprecations))) {
                $tokens[$index] = new Token([T_FUNCTION, $this->deprecations[$token->getContent()]]);
            }
        }
    }

    public function getPriority(): int
    {
        return 0;
    }

    protected function startsWith($haystack, $needles)
    {
        foreach ((array) $needles as $needle) {
            if ($needle !== '' && \substr($haystack, 0, \strlen($needle)) === (string) $needle) {
                return true;
            }
        }

        return false;
    }
}
