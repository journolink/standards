# JournoLink Standards

## Installation
Firstly, enter the repository details in the `"repositories"` section of your composer.json file:

```
"repositories": [
    {
        "type": "vcs",
        "url": "git@gitlab.com:journolink/standards"
    }
]   
```

Then run:

```bash
composer require --dev journolink/standards
```

## PSR
JournoLink broadly follows PSR-2 in code-style, with minor amendments:
* `use` statements ordered by length ascending
* Method chaining indented by one level
```php
$this->method()
    ->chain()
    ->result;
```
* `Arr::`/`Str::` used over `arr_`/`str_` (Laravel specific)

## PHP-CS-Fixer

.php_cs.dist containing style-guide ruleset available in vendor/journolink/standards/src/.php_cs.dist

Suggested usage is to create a composer script in the root composer.json file as follows:

```json
"scripts": {
    "standards": [
        "cp vendor/journolink/standards/src/.php_cs.dist ./.php_cs.dist",
        "vendor/bin/php-cs-fixer fix --dry-run --diff --show-progress=dots --allow-risky=yes --ansi"
    ]
}
```

Alternatively, run the following command in the project root to initialise the PHP-CS-Fixer standards:

```
$ cp vendor/journolink/standards/src/.php_cs.dist ./.php_cs.dist
```

Once initialised, PHP-CS-Fixer can be run with the following command:

```
$ vendor/bin/php-cs-fixer fix --dry-run --diff --show-progress=dots --allow-risky=yes --ansi
```

## General Standards

### Indentation
Code should be indented using 4 spaces, no tabs.

### Quotes
For consistency, single quotes should be used for strings. Double quotes should only be used where entity parsing is required, such as `\n, \r or \t`.

Where variables are required within a string, the use of `sprintf` is preferred.

### Control Structures
The use of `else` and `elseif` statements is discouraged and early `return` statements should be used where practical.

### Comments

#### Class-level comments
Class-level comments are not required in majority of cases and should only be included where absolutely necessary.
```php
/**
 * [Class description]
 *
 * @package [type]
 */
```

#### Function-level comments
All functions and methods MUST be prefixed with a function-level comment to indicate the purpose of the method.
```php
/**
 * [Method Description]
 *
 * @param [type] [$name] [description (optional)
 * @return [type]
 * @throws [type] 
 */
```

#### Inline comments
Any code block where the intention or output is not immediately obvious should be commented appropriately. Inline comments should be wrapped at 80 characters for readability.

### Method and variable naming
Methods and variables should be named in a clear and meaningful way, and should be in camelCase format (with the exception of constants, which should be named in all UPPERCASE format with underscores to separate words)
